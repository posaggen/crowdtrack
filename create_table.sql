create table POI(
	pid int NOT NULL AUTO_INCREMENT,
	name varchar(50),
	state varchar(50),
	city varchar(50),
	price int,
	category varchar(50),
	primary key (pid)
);

create table Users(
	login char(30),
	name varchar(40),
	userType boolean,
	password varchar(50),
	primary key (login)
);

create table VisEvent(
	vid int NOT NULL AUTO_INCREMENT,
	cost int,
	numberofheads int,
	primary key (vid)
);

create table Visit(
	login char(30),
	pid int,
	vid int,
	visitdate date,
	primary key (login, pid, vid),
	foreign key (login) references Users(login),
	foreign key (pid) references POI(pid),
	foreign key (vid) references VisEvent(vid)
);

create table Trust(
	login1 char(30),
	login2 char(30),
	isTrusted boolean,
	primary key (login1, login2),
	foreign key (login1) references Users(login),
	foreign key (login2) references Users(login)
);

CREATE TABLE Feedback (
	fid int NOT NULL AUTO_INCREMENT,
	pid int,
	login char(30),
	_text char(100),
	fbdate timestamp,
	score int,
	PRIMARY KEY(fid),
	FOREIGN KEY(pid) REFERENCES POI(pid),
	FOREIGN KEY(login) REFERENCES Users(login)
);

CREATE TABLE Rates (
	login char(30),
	fid int,
	rating int,
	PRIMARY KEY(login, fid),
	FOREIGN KEY(login) REFERENCES Users(login),
	FOrEIGN KEY(fid) REFERENCES Feedback(fid)
);

CREATE TABLE Favorites (
	pid int,
	login char(30),
	fvdate timestamp,
	PRIMARY KEY(pid, login),
	FOREIGN KEY(pid) REFERENCES POI(pid),
	FOREIGN KEY(login) REFERENCES Users(login)
);

CREATE TABLE Keywords (
	wid int NOT NULL AUTO_INCREMENT,
	word char(50),
	language char(20),
	PRIMARY KEY(wid)
);

CREATE TABLE HasKeywords (
	pid int,
	wid int,
	PRIMARY KEY(pid, wid),
	FOREIGN KEY(pid) REFERENCES POI(pid),
	FOREIGN KEY(wid) REFERENCES Keywords(wid)
);
