<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="acmdb.*, utils.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<% if (session.getAttribute("login") == null) { %>
			<jsp:forward page="index.jsp"/>
	<%	} %>

<%
	String name = (String)request.getParameter("name");
	String state = (String)request.getParameter("state");
	String city = (String)request.getParameter("city");
	int price = Integer.parseInt((String)request.getParameter("price"));
	String category = (String)request.getParameter("category");

	if (Checker.isValid(name) && Checker.isValid(state) 
			&& Checker.isValid(city) && Checker.isValid(category)) {
		Connector connector = new Connector();
		String insert = Query.insert("POI", new Object[]{"name", "state", "city", "price", "category"}
											, new Object[]{name, state, city, price, category});
		connector.stmt.execute(insert);
		connector.closeStatement();
		connector.closeConnection();
		response.setHeader("refresh", "0, URL=newPOI.jsp");
	} else {
		if (Checker.isValid(name) == false) {
			session.setAttribute("newPOI_Wrong_info", "name");
		} else if (Checker.isValid(state)) {
			session.setAttribute("newPOI_Wrong_info", "state");
		} else if (Checker.isValid(city)) {
			session.setAttribute("newPOI_Wrong_info", "city");
		} else if (Checker.isValid(category)) {
			session.setAttribute("newPOI_Wrong_info", "category");
		}
%>
		<jsp:forward page="newPOI.jsp"/>
<%
	}
	

%>



</body>
</html>