<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="../utils/jquery.js"></script>


<title>Create POI</title>
</head>
<body>

<%
	if (((String)session.getAttribute("login")).equals("admin") == false
		|| session.getAttribute("login") == null) {
%>
		<jsp:forward page="index.jsp"/>
<%
	}
%>

<% 
	if ((String)session.getAttribute("newPOI_Wrong_info") != null) {
%>
	<font color = "red"> 
		the format of <%=(String)session.getAttribute("newPOI_Wrong_info") %> is wrong 
	</font>
<%
		session.removeAttribute("newPOI_Wrong_info");
	}
%>

<h1> CREATE POI </h1>
	<form action="submitNewPOI.jsp" method="post">
		Name: <input type="text" name="name"> <br> 
		state: <input type="text" name="state"> <br>
		city: <input type="text" name="city"> <br>
		price: <input type="number" name="price"> <br>
		category: <input type="text" name="category"> <br>
		<input type="submit" value="submit"> <br> 
	</form>
	<a href="../homepage.jsp"> HomePage </a>
</body>
</html>