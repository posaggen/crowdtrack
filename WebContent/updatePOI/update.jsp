<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update POI</title>
</head>
<body>

<h1> Update POI </h1>

	<% if (session.getAttribute("login") == null) { %>
			<jsp:forward page="index.jsp"/>
	<%	} %>

<%
	int pid = Integer.parseInt((String)request.getParameter("pid"));
	String pname = (String)request.getParameter("name");
	String state = (String)request.getParameter("state");
	String city = (String)request.getParameter("city");
	int price = Integer.parseInt((String)request.getParameter("price"));
	String category = (String)request.getParameter("category");
%>
	<form action="updateData.jsp">
		<input type="hidden" name="pid", value='<%=pid%>'> <br> 
		Name: <input type="text" name="name", value='<%=pname%>'> <br> 
		State: <input type="text" name="state", value='<%=state%>'> <br>
		City: <input type="text" name="city", value='<%=city%>'> <br>
		Price: <input type="number" name="price", value='<%=price%>'> <br>
		category: <input type="text" name="category", value='<%=category%>'> <br>
		<input type="submit" value="Save Changes"> <br> 
	</form>
 	<a href= "<%= "../poiPage.jsp?pid="+pid%>" > Back To POI </a>
</body>
</html>