<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import = "acmdb.*, java.util.*, utils.*"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="script/jquery-1.11.1.min.js"></script>
<title>Personal Page</title>
</head>
<body>

<%
	if (session.getAttribute("login") == null) {
%>
		<jsp:forward page="index.jsp"/>
<%
	}
%>

<% 
	String name = (String)request.getParameter("uname");
	User user = new User(name);
	String myName = (String)session.getAttribute("login");
	int isTrusted = user.isTrustedBy(myName);
	if (user.exists()){
%>
<h1> User name: <%= user.getName()%> </h1>


	<div id = "trustblock">
		<input type="button" id="trust" value="Trust" onclick="trust()">
		<input type="button" id="distrust" value="Distrust" onclick="distrust()">
		<input type="button" id="reset" value="reset" onclick="clearTrust()">
	</div><br>
	
	
	<b>Degrees of separation with me:</b><br><br>
	<%
		Pair<Integer, List<Pair<String, String>>> ret = user.getDegree(myName);
		if (ret.first == 0){
			out.println("Not 1-degree nor 2-degree neither.<br>");
		} else if (ret.first == 1){
			out.println("1-degree, our common favorited POI:<br>");
			for (Pair<String, String> element : ret.second){
				out.println("<a href = \"poiPage.jsp?pid=" + element.first + "\">" + element.second + "</a><br>");
			}
		} else {
			out.println("2-degree, our common 1-degree user:<br>");
			for (Pair<String, String> element : ret.second){
				out.println("<a href = \"personalPage.jsp?uname=" + element.first + "\">" + element.second + "</a><br>");
			}
		}
	%>
	<br><br>
	
	
	<a href="homepage.jsp"> HomePage </a>
	
	
	
	
	<%} else {out.print("No such user!!");} %>
	
	
	
	
	<script LANGUAGE="javascript">
	function distrust(){
		$.ajax({
			url : 'trust/trust.jsp',
			data : {
				login1 : <%="\"" + myName + "\""%>,
				login2 : <%="\"" + name + "\""%>,
				isTrusted : "0"
			},
			async : false,
			success : function(data) {
			},
		});
		$("#trust").show();
		$("#distrust").hide();
	}
	
	function trust(){
		$.ajax({
			url : 'trust/trust.jsp',
			data : {
				login1 : <%="\"" + myName + "\""%>,
				login2 : <%="\"" + name + "\""%>,
				isTrusted : "1"
			},
			async : false,
			success : function(data) {
			},
		});
		$("#trust").hide();
		$("#distrust").show();
	}
	
	function clearTrust(){
		$.ajax({
			url : 'trust/trust.jsp',
			data : {
				login1 : <%="\"" + myName + "\""%>,
				login2 : <%="\"" + name + "\""%>,
				isTrusted : "-1"
			},
			async : false,
			success : function(data) {
			},
		});
		$("#trust").show();
		$("#distrust").show();
	}
	
	$(document).ready(function() {
		if (<%="\"" + myName + "\""%> != <%="\"" + name + "\""%>){
			if (<%=isTrusted%> == 1){
				$("#trust").hide();
			} else if (<%=isTrusted%> == 0){
				$("#distrust").hide()
			}
		} else {
			$("#trustblock").hide();
		}
		
	});
	</script>
</body>
</html>