<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import = "java.util.*, utils.*, acmdb.*"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Statistic</title>
</head>
<body>
<a href="../homepage.jsp"> HomePage </a><br><br>
<%
Stat st = new Stat();
%>
Top 5 most popular POIs for each category:<br><br>
<div>
    <table id="popular" class="table">
        <thead>
            <tr>
            	<th>Name</th>
                <th>Category</th>
				<th>#Visits</th>
            </tr>
        </thead>
        <tbody>
        <%
        List<HashMap<String, String>> pop = st.getPop();
        for (HashMap<String, String> element : pop){
        	out.print("<tr>\n");
        	out.print("<td> <a href=\"../poiPage.jsp?pid=" + element.get("pid") + "\">"+ element.get("name") + "</td>\n");
        	out.print("<td> " + element.get("category") + "</td>\n");
        	out.print("<td> " + element.get("visits") + "</td>\n");
        }
        %>
        </tbody>
	</table>
</div><br><br><br>

Top 5 most expensive POIs for each category:<br><br>
<div>
    <table id="cost" class="table">
        <thead>
            <tr>
            	<th>Name</th>
                <th>Category</th>
				<th>Cost</th>
            </tr>
        </thead>
        <tbody>
        <%
        List<HashMap<String, String>> cost = st.getCost();
        for (HashMap<String, String> element : cost){
        	out.print("<tr>\n");
        	out.print("<td> <a href=\"../poiPage.jsp?pid=" + element.get("pid") + "\">"+ element.get("name") + "</td>\n");
        	out.print("<td> " + element.get("category") + "</td>\n");
        	out.print("<td> " + element.get("cost") + "</td>\n");
        }
        %>
        </tbody>
	</table>
</div><br><br><br>

Top 5 most highly rated POIs for each category:<br><br>
<div>
    <table id="score" class="table">
        <thead>
            <tr>
            	<th>Name</th>
                <th>Category</th>
				<th>Rating</th>
            </tr>
        </thead>
        <tbody>
        <%
        List<HashMap<String, String>> score = st.getScore();
        for (HashMap<String, String> element : score){
        	out.print("<tr>\n");
        	out.print("<td> <a href=\"../poiPage.jsp?pid=" + element.get("pid") + "\">"+ element.get("name") + "</td>\n");
        	out.print("<td> " + element.get("category") + "</td>\n");
        	out.print("<td> " + element.get("score") + "</td>\n");
        }
        %>
        </tbody>
	</table>
</div><br><br><br>

</body>
</html>