<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import = "acmdb.*, java.util.*, utils.*"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="../script/jquery-1.11.1.min.js"></script>
<title>User Ranking</title>
</head>
<body>
<a href="../homepage.jsp"> HomePage </a>
<br><br><br>
Top <input type = "text" style="width: 30px" id = "tm"> most trusted user:
<input type="button" id="tms" value="Search" onclick="searchTrust()">
<br>
<div>
    <table id="ttrust" class="table">
        <thead>
            <tr>
            	<th>Rank</th>
                <th>User Name</th>
				<th>Score</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
	</table>
</div>
<br><br><br>

Top <input type = "text" style="width: 30px" id = "um"> most useful user:
<input type="button" id="ums" value="Search" onclick="searchUseful()">
<br>
<div>
    <table id="tuseful" class="table">
        <thead>
            <tr>
            	<th>Rank</th>
                <th>User Name</th>
				<th>Score</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
	</table>
</div>

	<script LANGUAGE="javascript">

	function userLink(login, name) {
	    return $("<a>").attr("href", "../personalPage.jsp?uname=" + login).text(name);
	}
	
	function searchTrust(){
		var m = $("#tm").val();
		if (isNaN(m)){
			alert("number required!");
			return;
		}
		$.ajax({
			url : 'ranking.jsp',
			data : {
				m : m,
				type : 0
			},
			success : function(data) {
				$('#ttrust tbody tr').remove();
				var ret = eval("(" + data + ")");
				var $sr = $('#ttrust tbody');
				for (var i = 0; i < ret.length; ++i){
					$sr.append($("<tr>").append($("<td>").text(i + 1))
							            .append($("<td>").append(userLink(ret[i].login, ret[i].name)))
							            .append($("<td>").append(ret[i].score))
							  );
				}
			},
		});
	}
	

	function searchUseful(){
		var m = $("#um").val();
		if (isNaN(m)){
			alert("number required!");
			return;
		}
		$.ajax({
			url : 'ranking.jsp',
			data : {
				m : m,
				type : 1
			},
			success : function(data) {
				$('#tuseful tbody tr').remove();
				var ret = eval("(" + data + ")");
				var $sr = $('#tuseful tbody');
				for (var i = 0; i < ret.length; ++i){
					$sr.append($("<tr>").append($("<td>").text(i + 1))
							            .append($("<td>").append(userLink(ret[i].login, ret[i].name)))
							            .append($("<td>").append(ret[i].score))
							  );
				}
			},
		});
	}

	
	</script>

</body>
</html>