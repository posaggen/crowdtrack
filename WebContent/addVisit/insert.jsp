<%@ page language="java" import="acmdb.*, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<a href="../homepage.jsp"> HomePage </a><br><br>

<body>
<% if (session.getAttribute("login") == null) { %>
			<jsp:forward page="index.jsp"/>
	<%	} %>

<%
	String poi = (String)request.getParameter("where");
	int cost = Integer.parseInt((String)request.getParameter("cost"));
	int numberofheads = Integer.parseInt((String)request.getParameter("numberofheads"));
	String date = (String)request.getParameter("date");
	int pid = Integer.parseInt((String)request.getParameter("pid"));
	VisitChecker checker = new VisitChecker((String)session.getAttribute("login"), poi, cost, numberofheads, date);
	checker.pid = pid;
	checker.insert();	
%>


Insert successed!<br><br>
Other people visited <b><%=poi %></b> also visited:<br><br>
<%
Recommender rec = new Recommender();
List<HashMap<String, String>> ans = rec.recommend(pid);
if (ans.size() == 0){
	out.print("Sadly, no recommendation..");
} else {%>
<div>
    <table id="recommend" class="table">
        <thead>
            <tr>
            	<th>Name</th>
				<th>#Visits</th>
            </tr>
        </thead>
        <tbody>
        <%
        for (HashMap<String, String> element : ans){
        	out.print("<tr>\n");
        	out.print("<td> <a href=\"../poiPage.jsp?pid=" + element.get("pid") + "\">"+ element.get("name") + "</td>\n");
        	out.print("<td> " + element.get("visits") + "</td>\n");
        }
        %>
        </tbody>
	</table>
</div><br><br><br>
<%	
}
%>



</body>
</html>