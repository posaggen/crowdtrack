<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add Visit</title>
</head>
<body>

<% if (session.getAttribute("login") == null) { %>
			<jsp:forward page="index.jsp"/>
	<%	} %>

<%
	if (session.getAttribute("addVisitFlag") != null) {
%>
		<font color = "red"> Invalid POI!</font>
<%
		session.removeAttribute("addVisitFlag");
	}
%>	 

	<form action="checkAndConfirm.jsp" method="post">
		Where? <input type="text" name="where"> <br> 
		When? <input type="date" name="date"> <br>
		How much? <input type="number" name="cost"> <br>
		How many people? <input type="number" name="numberofheads"> <br>
		<input type="submit" value="confirm"> <br> 
	</form>

	<a href="../homepage.jsp"> HomePage </a>

</body>
</html>