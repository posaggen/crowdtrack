<%@ page language="java" import="acmdb.*" import="utils.*"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> Confirm? </title>
</head>

<body>
<% if (session.getAttribute("login") == null) { %>
			<jsp:forward page="index.jsp"/>
	<%	} %>

<%
	String poi = (String)request.getParameter("where");
	int cost = Integer.parseInt((String)request.getParameter("cost"));
	int numberofheads = Integer.parseInt((String)request.getParameter("numberofheads"));
	String date = (String)request.getParameter("date");
%>

<%
	VisitChecker checker = new VisitChecker((String)session.getAttribute("login"), poi, cost, numberofheads, date);
	if (checker.checkPOI() == true) {
	} else {
		session.setAttribute("addVisitFlag", 1);
%>
		<jsp:forward page="addVisit.jsp"/>
<%
	}
%>

	Are you sure you want to add this visit? <br>
	Location: <%= poi %> <br>
	date: <%= date %> <br>
	numberofheads: <%= numberofheads %> <br>
	cost: <%= cost %> <br>
	
	<form action='insert.jsp' method="post"> 
		<p><h3>确认提交这些信息吗？</h3></p> 
		<input type="hidden" name="where", value='<%=poi%>' > 
		<input type="hidden" name="date", value='<%=date%>'>
		<input type="hidden" name="cost", value='<%=cost%>'>
		<input type="hidden" name="numberofheads", value='<%=numberofheads%>'>
		<input type="hidden" name="pid", value='<%=checker.pid%>'>
		<input type="submit" value="confirm"> <br> 
	</form>

	<a href="../homepage.jsp"> HomePage </a>
</body>
</html>