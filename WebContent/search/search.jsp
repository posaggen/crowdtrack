<%@ page language="java" pageEncoding="UTF-8"%>
<html>
<head>
<title>Where to go!</title>
<script src="../script/jquery-1.11.1.min.js"></script>
</head>

<body>

	price range:
	<input type="text" id="lprice" style="width: 50px"> -
	<input type="text" id="rprice" style="width: 50px">
	<br>
	<br> address:
	<select id="position">
		<option value="city">city</option>
		<option value="state">state</option>
	</select>
	<input type="text" id="posValue">
	<br>
	<br> category:
	<input type="text" id="category">
	<br>
	<br> keywords:
	<input type="text" id="keywords">(separated by ';')
	<br>
	<br> sorted by:
	<select id="sort">
		<option value="price">price</option>
		<option value="avgs">Avg score</option>
		<option value="avgts">Avg trusted score</option>
	</select>
	<br>
	<br>
	<input type="button" id="submit" value="Submit" onclick="submit()">
	<div>
    <table id="search-result" class="table">
        <thead>
            <tr>
            	<th>#</th>
            	<th>Name</th>
            	<th>Price</th>
            	<th>Average Score</th>
            	<th>Average Trusted Score</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
	</table>
	</div>
	<div id = "er">
	</div>
	<br><br><br>
	<a href="../homepage.jsp"> HomePage </a>
	<script LANGUAGE="javascript">
		function poiLink(pid, name) {
	    	return $("<a>").attr("href", "../poiPage.jsp?pid=" + pid).text(name);
		}
		function submit() {
			var p1 = $('#lprice').val();
			var p2 = $('#rprice').val();
			var p3 = $('#position').val();
			var p4 = $('#posValue').val();
			var p5 = $('#sort').val();
			var p6 = $('#category').val();
			var p7 = $('#keywords').val();
			var p8 = "<%=session.getAttribute("login")%>";
			$.ajax({
				url : 'doSearch.jsp',
				data : {
					lprice : p1,
					rprice : p2,
					position : p3,
					posValue : p4,
					sort : p5,
					category : p6,
					keywords : p7,
					uname : p8
				},
				success : function(data) {
					var ret = eval("(" + data + ")");
					$('#search-result tbody tr').remove();
					document.getElementById('er').innerHTML = "";
					var $sr = $('#search-result tbody');
					var $err = $('#er');
					if (ret.length > 0) {
						if (!("error" in ret[0])) {
							for (var i = 0; i < ret.length; ++i) {
								$sr.append($("<tr>").append($("<td>").text(i + 1))
							            .append(poiLink(ret[i].pid, ret[i].name))
							            .append($("<td>").text(ret[i].price))
							            .append($("<td>").text(ret[i].avgs))
							            .append($("<td>").text(ret[i].avgts))
							  );
							}
						} else {
							$err.append("invalid search query!");
						}
					} else {
						$err.append("no matching POI!");
					}
				},
			});
			return true;
		}
	</script>
</body>
</html>