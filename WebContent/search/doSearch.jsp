<%@ page language="java" pageEncoding="UTF-8" import="acmdb.*, utils.*, java.util.*"%><%
		String lprice = Fix.fix((String) request.getParameter("lprice"));
		String rprice = Fix.fix((String) request.getParameter("rprice"));
		String position = Fix.fix((String) request.getParameter("position"));
		String posValue = Fix.fix((String) request.getParameter("posValue"));
		String sort = Fix.fix((String) request.getParameter("sort"));
		String category = Fix.fix((String) request.getParameter("category"));
		String keywords = Fix.fix((String) request.getParameter("keywords"));
		String uname = Fix.fix((String) request.getParameter("uname"));
		Connector connector = new Connector();
		POISearcher searcher = new POISearcher();
		List<HashMap<String, String>> ans = searcher.search(lprice, rprice, position, posValue, sort, category,
				keywords, uname, connector.stmt, false);
		connector.closeStatement();
		connector.closeConnection();
		if (ans == null){
			out.print("[{\"error\":0}]");
		} else {
			out.print(Json.toJson(ans));
		}
%>