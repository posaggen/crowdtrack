<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import = "acmdb.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Crowd Tracker!</title>
</head>
<body>

	<h1> Crowd Tracker! </h1>
	
	<% if (session.getAttribute("login") == null) { %>
			<jsp:forward page="index.jsp"/>
	<%	} %>
	
	<%
	String login = (String)session.getAttribute("login");
	User user = new User(login);
	%>
	
	Welcome <%= user.getName() %>  <br>
	<a href = "log/logout.jsp"> logout </a>
	
	<br>
	<br>

<%
	if (((String)session.getAttribute("login")).equals("admin")) {
%>
		
		<a href="newPOI/newPOI.jsp"> create a POI</a>
		<br>
<% 
	} 
%>

	<a href="award/userRanking.jsp"> User Ranking </a><br>

	<a href="personalPage.jsp?uname=<%=session.getAttribute("login") %>"> Personal Page </a><br>

	<a href="addVisit/addVisit.jsp"> add visit! </a> <br>

	Search some users here!
	<form action="personalPage.jsp" method="post">
		Username: <input type="text" name="uname"><br> 
		<input type="submit">
	</form>
	
	<br>
	<br>
	<br>
	
	Search some POIs here!
	<form action="poiPage.jsp" method="post">
		POI: <input type="text" name="name"><br> 
		<input type="submit">
	</form>
	
	<br>
	<br>
	<br>
		
	<a href="search/search.jsp"> Search for POI </a>
	
	<br>
	
	<a href="statistic/statistic.jsp"> Statistic</a>
	
	
</body>
</html>