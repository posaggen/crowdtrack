<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import = "acmdb.*, utils.*" %>
<%@ page import="java.sql.ResultSet, java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="script/jquery-1.11.1.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>POI Page</title>
</head>
<body>

<% 
	Connector connector = new Connector();
	ResultSet result = null;
	int pid = 0; 
	String pname = "", state = "", city = "", category = "";
	int price = 0;
	
	boolean success = true;
	if (request.getParameter("pid") != null) {
		pid = Integer.parseInt((String)request.getParameter("pid"));
		String query = "Select * from POI where pid" + "=" + pid;
		result = connector.stmt.executeQuery(query);
		if (result.next()) {
			pname = result.getString("name");
		} else {
			success = false;
		}
	} else if (request.getParameter("name") != null) {
		pname = (String)request.getParameter("name");
		if (!Checker.isValid(pname)) {
			success = false;
		} else {
			String query = "Select * from POI where name" + "='" + pname + "'";
			result = connector.stmt.executeQuery(query);
			if (result.next()) {
				pid = result.getInt("pid");
			} else {
				success = false;
			}
		}
	} else {
		success = false;
	}
	
	if (!success) {		
%>
		<font color="red"> Invalid POI's id or POI's name </font>
<%

		connector.closeStatement();
		connector.closeConnection();
	} else {
		state = result.getString("state");
		city = result.getString("city");
		category = result.getString("category");
		price = result.getInt("price");
		
		connector.closeStatement();
		connector.closeConnection();
%>

<h1> <%= pname %> </h1> <br>
state: <%= state %> <br>
city: <%= city %> <br>
category: <%= category %> <br>  
price: <%= price %> <br>

<%
	if (session.getAttribute("login") != null && ((String)session.getAttribute("login")).equals("admin")) {
%>
		<form action="updatePOI/update.jsp", method="post">
			<input type="hidden" name="pid", value='<%=pid%>'>
			<input type="hidden" name="name", value='<%=pname%>'>
			<input type="hidden" name="state", value='<%=state%>'>
			<input type="hidden" name="city", value='<%=city%>'>
			<input type="hidden" name="price", value='<%=price%>'>
			<input type="hidden" name="category", value='<%=category%>'>
			<input type="submit" value="Update POI"> 
		</form>
<%
	}
String login = (String)session.getAttribute("login");
	if (session.getAttribute("login") != null) {
		if (Favorite.check(login, pid)) {
%>		

		<input id = "pid1", type="hidden" value='<%=pid%>'>
		<button type="button" onclick="delFavorite()"> ❤️ </button>
<script language="javascript">
	function delFavorite() {
		var v = $('#pid1').val();
		$.ajax({
			url: 'favorite/favorite.jsp',
			data: {
				pid : v,
				type : 'del'
			},
			success : function(data) {
				location.reload(true); 
			}
		}); 
		return true;
	}
</script>
<%		
		} else {   
%>
		<input id = "pid2", type="hidden" value='<%=pid%>'>
		<button type="button" onclick="insFavorite()"> ♡ </button>
<script language="javascript">
	function insFavorite() {
		var v = $('#pid2').val();
		$.ajax({
			url: 'favorite/favorite.jsp',
			data: {
				pid : v,
				type : 'ins'
			},
			success : function(data) {
				location.reload(true);
			}
		});
		return true;
	}
</script>
<%
		}
%>

<div>
	<table id="o_feedbacks" class="table">
        <thead>
            <tr>
            	<th> Rank </th>
            	<th> User </th>
                <th> Comment </th>
				<th> Score </th>
				<th> Avg_rating </th>
				<th> Date </th>
				<th> Extra </th>
            </tr>
        </thead>
        <tbody>
<%
		List<FB> others = FeedBack.getList(pid);
		for (int i = 0; i < others.size(); i++) {
			%>
			<tr>
			<td> <%= i + 1 %></td>
			<td> <%= "<a href='personalPage.jsp?uname=" + others.get(i).login + "'>" + others.get(i).name + "</a>" %> </td>
			<td> <%= others.get(i).text %> </td>
			<td> <%= others.get(i).score %></td>
			<td> <%= others.get(i).avg_rating %></td>
			<td> <%= others.get(i).date %> </td>
			<td>
						<select id="<%= "rating_" + i %>">
						
			<%
				if (!others.get(i).login.equals(login)) {
					%>
							<option value=0>useless</option>
				<%} %>
							<option value=1>useful</option>
							<option value=2>very useful</option>
						</select>
						<input id="<%= "fid_" + i %>" type = "hidden" value=<%= others.get(i).fid %> >
						<input id="<%= "login_" + i %>" type = "hidden" value=<%= login %> >
						<input type="button" id="submit" value="Submit" onclick="submit_rating(<%=i%>)">
				
			</td>
			</tr>
			<%
		}
%>
        </tbody>
	</table>
</div>

<script language="javascript">
	function submit_rating(id) {
		var v1 = $('#rating_' + id).val();
		var v2 = $('#fid_' + id).val();
		var v3 = $('#login_' + id).val();
		$.ajax({
			url: 'feedback/submitRating.jsp',
			data: {
				fid : v2,
				rating : v1,
				who : v3 
			},
			success : function(data) {
				var ret = eval("(" + data + ")");
				if (("error" in ret[0])) {
					alert("You have commented this feedback!");
				} else {
					location.reload(true);
				}
			}
		});
		return true;
	}
</script>

<br>
<%		if (FeedBack.isExist(pid, login) == false) { %>
			comments: <input id="fb1" type="text"/> &nbsp;&nbsp;&nbsp;
			score: <input id="fb2" type="number" min="0" max="10"/> <br> <br>
			<input id="fb3" type="hidden" value='<%= pid %>' />
			<button type="button" onclick="submitFeedBack()"> Submit </button>
<%		} %>
<script language="javascript">
	function submitFeedBack() {
		var v1 = $('#fb1').val();
		var v2 = $('#fb2').val();
		var v3 = $('#fb3').val();
		$.ajax({
			url: 'feedback/submitFeedback.jsp',
			data: {
				_text : v1,
				score : v2,
				pid : v3
			},
			success : function(data) {
				location.reload(true);
			}
		});
		return true;
	}
</script>
<%
	}
}
%>
	<br>
	<br>
	<a href="homepage.jsp"> HomePage </a>
</body>
</html>