<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="acmdb.*, utils.*, java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> Favorite </title>
</head>
<body>
	<% if (session.getAttribute("login") == null) { %>
			<jsp:forward page="../index.jsp"/>
	<%	} %>

<%
	int pid = Integer.parseInt(Fix.fix((String) request.getParameter("pid")));
	String type = Fix.fix((String) request.getParameter("type"));
	Connector connector = new Connector();
	Statement stmt = connector.stmt;
	
	String login = (String)session.getAttribute("login");
	
	String query = "";
	if (type.equals("ins")) {
		query = Query.insert("Favorites", new Object[]{"pid", "login"}, new Object[]{pid, login});
	}
	if (type.equals("del")) {
		query = "delete from Favorites where pid = " + pid + " and login = '" + login + "'"; 
	}
	
	try{
		stmt.execute(query);
	} catch(Exception e){
		System.err.println("Unable to execute query:"+query+"\n");
        System.err.println(e.getMessage());
        throw(e);
	}
	
	connector.closeStatement();
	connector.closeConnection();
	response.setHeader("refresh", "0, URL=../poiPage.jsp?pid=" + pid);	
%>
</body>
</html>