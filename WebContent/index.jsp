<%@ page language="java" import="acmdb.*"%>
<html>
<head>
<title>Welcome to CrowdTracker!</title>
</head>

<body>

	<h1>Crowd Tracker!</h1>

	<%
		if (session.getAttribute("login") == null){
			if (session.getAttribute("loginFlag") != null){
				session.removeAttribute("loginFlag");
	%>
	<font color = "red"> Incorrect username or password!</font>
	<%
			}
	%>

	<form action="log/login.jsp" method="post">
		Username: <input type="text" name="uname"><br> Password:
		<input type="password" name="upass"><br> 
		<input type="submit" value="login">
	</form>
	<a href = "log/register.jsp"> Register </a>
	
	<%
		}else{
			response.setHeader("refresh", "0, URL=homepage.jsp");	
		}
	%>
</body>