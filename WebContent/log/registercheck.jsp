<%@ page language="java" import="acmdb.*"%>
<html>
<head>
<title> Login </title>
</head>

<body>
<%  
    String name = (String)request.getParameter("uname");
	String nickname = (String)request.getParameter("nickname");
    String password = (String)request.getParameter("upass");
    boolean flag = true;

	Connector connector = new Connector();
	RegisterChecker checker = new RegisterChecker();
	int state = checker.check(name, nickname, password, connector.stmt);
	if (state == 1) {
		session.setAttribute("login", name);
		response.setHeader("refresh", "0, URL=../index.jsp");  
	} else {
		session.setAttribute("regFlag", state);
		response.setHeader("refresh", "0, URL=register.jsp");
	}
	connector.closeStatement();
	connector.closeConnection();
%>

</body>