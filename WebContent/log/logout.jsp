<%@ page language="java" import="acmdb.*"%>
<html>
<head>
<title> Logout </title>
</head>

<body>
<%  
	session.removeAttribute("login");
	session.removeAttribute("loginFlag");
	response.setHeader("refresh", "0, URL=../index.jsp");
%>

</body>