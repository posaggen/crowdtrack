<%@ page language="java" import="acmdb.*"%>
<html>
<head>
<title> Login </title>
</head>

<body>
<%  
    String name = (String)request.getParameter("uname");  
    String password = (String)request.getParameter("upass");  
	Connector connector = new Connector();
	LoginChecker login = new LoginChecker();
	if (login.checkLogin("login", name, password, connector.stmt)){
		session.setAttribute("login", name);
		response.setHeader("refresh", "0, URL=../homepage.jsp");  
	} else {
		session.setAttribute("loginFlag", 0);
		response.setHeader("refresh", "0, URL=../index.jsp");
	}
	connector.closeStatement();
	connector.closeConnection();
%>

</body>