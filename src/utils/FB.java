package utils;

import acmdb.LoginChecker;

public class FB {
	public String text;
	public String date;
	public int score;
	public double avg_rating;
	public int fid;
	public String login;
	public String name;
	
	public FB(String name, String _text, String _date, int _score, double _avg_rating, int _fid, String _login) {
		this.name = name;
		text = _text;
		date = _date;
		score = _score;
		avg_rating = _avg_rating;
		fid = _fid;
		login = _login;
	}
};
