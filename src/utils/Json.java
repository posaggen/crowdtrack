package utils;

import java.util.*;

public class Json {
	public Json() {
	}

	public static String getString(String u){
		return "\"" + u + "\"";
	}
	
	public static String get(String attribute, String obj){
		return getString(attribute) + ":" + getString(obj);
	}
	

	public static String get(String attribute, Double obj){
		return getString(attribute) + ":" + Double.toString(obj);
	}
	
	public static String get(String attribute, int obj){
		return getString(attribute) + ":" + Integer.toString(obj);
	}
	
	
	public static String toJson(List<HashMap<String, String>> list) {
		String ans = "[";
		boolean isFirst = true;
		for (HashMap<String, String> element : list){
			if (!isFirst) ans += ",";
			isFirst = false;
			ans += "{";
			ans += getString("pid") + ":" + getString(element.get("pid"));
			ans += ",";
			ans += getString("name") + ":" + getString(element.get("name"));
			ans += ",";
			ans += getString("price") + ":" + getString(element.get("price"));
			ans += ",";
			ans += getString("avgs") + ":" + getString(element.get("avgs"));
			ans += ",";
			ans += getString("avgts") + ":" + getString(element.get("avgts"));
			ans += "}";
		}
		ans += "]";
		return ans;
	}
	
	public static void main(String args[]){
		ArrayList<Pair<Integer, String>> list = new ArrayList<Pair<Integer, String>>();
	}
}