package utils;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

import acmdb.Connector;

public class Query {
	public Query() {
	}

	public static String insert(String tableName, Object[] list) {
		String res = "insert into " + tableName + " values (";
		boolean isFirst = true;
		for (Object element : list) {
			if (!isFirst) {
				res += ", ";
			} else
				isFirst = false;
			if (element instanceof Integer) {
				res += ((Integer) element).toString();
			}
			if (element instanceof String){
				res += "'" + (String)element + "'";
			}
		}
		res += ")";
		return res;
	}
	
	public static String insert(String tableName, Object[] attr, Object[] list) {
		String res = "insert into " + tableName + "(";
		boolean isFirst = true;
		for (Object element: attr) {
			if (!isFirst) {
				res += ", ";
			} else {
				isFirst = false;
			}
			res += (String)element;
		}
		
		
		res = res + ") values (";
		
		isFirst = true;
		for (Object element : list) {
			if (!isFirst) {
				res += ", ";
			} else
				isFirst = false;
			if (element instanceof Integer) {
				res += ((Integer) element).toString();
			}
			if (element instanceof String){
				res += "'" + (String)element + "'";
			}
		}
		res += ")";
		return res;
	}
	
	public static String update(String tableName, String[] attr, Object[] value, String where) {
		String res = "update " + tableName + " set ";
		
		boolean isFirst = true;
		for (int i = 0; i < attr.length; i++) {
			if (!isFirst) {
				res = res + ", ";
			} else {
				isFirst = false;
			}
			
			res = res + attr[i] + " = ";
			if (value[i] instanceof String) {
				res = res + "'" + (String)value[i] + "'";
			}
			if (value[i] instanceof Integer) {
				res = res + "'" + (Integer)value[i] + "'";
			}
		}
		res = res + " where " + where;
		return res;
	}
}
