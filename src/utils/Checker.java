package utils;

import java.util.*;

public class Checker {
	public Checker() {
	}
	
	public static boolean isValid(String buf){
		for (int i = 0; i < buf.length(); ++i){
			char ch = buf.charAt(i);
			if ((!Character.isDigit(ch)) && (!Character.isLetter(ch)) && (ch != '_') && (ch != ' ')){
    			return false;
    		}
		}
		return true;
	}

	public static boolean isNumber(String buf) {
		for (int i = 0; i < buf.length(); ++i){
			char ch = buf.charAt(i);
			if (ch < '0' || ch > '9'){
				return false;
			}
		}
		return true;
	}
}