package acmdb;

import java.sql.*;
import java.util.*;
import utils.*;
//import javax.servlet.http.*;

public class POISearcher {

	public POISearcher() {
	}

	public List<HashMap<String, String>> search(String lprice, String rprice, String position, String posValue,
			String sort, String category, String keywords, String uname, Statement stmt, boolean isDebug)
			throws Exception {

		if (lprice.length() > 0 && !Checker.isNumber(lprice)) {
			return null;
		}

		if (rprice.length() > 0 && !Checker.isNumber(rprice)) {
			return null;
		}

		if (!Checker.isValid(position) || !Checker.isValid(posValue) || !Checker.isValid(category)) {
			return null;
		}

		String query = "";
		ResultSet results;
		boolean flag = false;
		if (lprice.length() > 0) {
			query += "p.price >= " + lprice;
			flag = true;
		}
		if (rprice.length() > 0) {
			if (flag)
				query += " and ";
			query += "p.price <= " + rprice;
			flag = true;
		}
		if (posValue.length() > 0) {
			if (flag)
				query += " and ";
			if (position.equals("city")) {
				query += "p.city = '" + posValue + "'";
			} else {
				query += "p.state = '" + posValue + "'";
			}
			flag = true;
		}
		if (category.length() > 0) {
			if (flag)
				query += " and ";
			query += "p.category = '" + category + "'";
			flag = true;
		}
		if (keywords.length() > 0) {
			if (flag)
				query += " and ";
			query += "hk.pid = p.pid and (";
			String[] keys = keywords.split(";");
			for (int i = 0; i < keys.length; ++i) {
				if (i > 0)
					query += " or ";
				if (!Checker.isValid(keys[i])) {
					return null;
				}
				query += "(select k.word from Keywords k where k.wid = hk.wid) = '" + keys[i] + "'";
			}
			query += ")";
		}
		if (keywords.length() > 0) {
			query = "select distinct p.*, (select avg(f.score) from feedback f where f.pid = p.pid) as avgs, "
					+ "(select avg(f.score) from feedback f, trust t where "
					+ "f.pid = p.pid and t.login2 = f.login and t.isTrusted = 1 and "
					+ "t.login1 = '" + uname + "') as avgts"
					+ " from POI p, HasKeyWords hk where " + query;
		} else {
			query = "select distinct p.*, (select avg(f.score) from feedback f where f.pid = p.pid) as avgs, "
					+ "(select avg(f.score) from feedback f, trust t where "
					+ "f.pid = p.pid and t.login2 = f.login and t.isTrusted = 1 and "
					+ "t.login1 = '" + uname + "') as avgts"
					+ " from POI p where " + query;
		}
		if (sort.equals("price")) {
			query += " order by p.price";
		}
		if (sort.equals("avgs")) {
			query += " order by avgs DESC";
		}
		if (sort.equals("avgts")) {
			query += " order by avgts DESC";
		}

		

		try {
			results = stmt.executeQuery(query);
		} catch (Exception e) {
			System.err.println("Unable to execute query:" + query + "\n");
			System.err.println(e.getMessage());
			throw (e);
		}
		List<HashMap<String, String>> ans = new ArrayList<HashMap<String, String>>();
		while (results.next()) {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("name", results.getString("name"));
			map.put("pid", results.getString("pid"));
			map.put("price", Integer.toString(results.getInt("price")));
			map.put("avgs", Double.toString(results.getDouble("avgs")));
			map.put("avgts", Double.toString(results.getDouble("avgts")));
			ans.add(map);
		}
		return ans;
	}

}
