package acmdb;

import utils.*;
import java.util.*;
import java.sql.*;
//import javax.servlet.http.*;

public class RegisterChecker{
	
	public RegisterChecker() {
	}
	
	public int check(String name, String nickname, String password, Statement stmt) throws Exception{
		boolean flag = true;
		if (name.length() == 0 || nickname.length() == 0 || password.length() == 0){
			flag = false;
		}
	    if (name.length() > 30 || nickname.length() > 40 || password.length() > 16){
	    	flag = false;
	    } else {
	    	flag &= Checker.isValid(name);
	    	flag &= Checker.isValid(nickname);
	    	flag &= Checker.isValid(password);
	    }
	    if (flag){
	    	String query;
	    	ResultSet results; 
			query="Select * from Users where login ='" + name + "'";
			try{
				results = stmt.executeQuery(query);
	        } catch(Exception e) {
				System.err.println("Unable to execute query:"+query+"\n");
		                System.err.println(e.getMessage());
				throw(e);
			}
			if (!results.next()){
				query = Query.insert("Users", new Object[]{name, nickname, 0, password});
				try{
					stmt.execute(query);
				} catch(Exception e){
					System.err.println("Unable to execute query:"+query+"\n");
	                System.err.println(e.getMessage());
	                throw(e);	
				}
				return 1;
			} else {
				return -1;
			}
	    }
	    return 0;
	}
	
}
