package acmdb;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import utils.FB;
import utils.Pair;
import utils.Query;

import java.sql.*;

public class FeedBack {
	public FeedBack() {
		
	}
	
	public static boolean isExist(int pid, String login) throws Exception { 
		Connector connector = new Connector();
		String query = "select * from Feedback "
				+ "where pid = " + pid + " and login = '" + login + "'";
		ResultSet result;
		try{
			result = connector.stmt.executeQuery(query);
        } catch(Exception e) {
			System.err.println("Unable to execute query:"+query+"\n");
	        System.err.println(e.getMessage());
			throw(e);
		}
		
		boolean ret = false;
		if (result.next()) {
			ret = true;;
		}
		connector.closeStatement();
		connector.closeConnection();
		return ret;
	}
	
	public static boolean insert(int pid, String login, int score, String text) throws Exception {
		Connector connector = new Connector();
		String query = Query.insert("Feedback", new Object[]{"pid", "login", "_text", "score"}
									, new Object[]{pid, login, text, score});
		try{
			connector.stmt.execute(query);
        } catch(Exception e) {
			System.err.println("Unable to execute query:"+query+"\n");
	        System.err.println(e.getMessage());
			throw(e);
		}
		return true;
	}
	
	public static String who_is_the_author(int fid) throws Exception {
		Connector connector = new Connector();
		String query = "select * from Feedback "
				+ "where fid = " + fid;
		ResultSet result;
		try{
			result = connector.stmt.executeQuery(query);
        } catch(Exception e) {
			System.err.println("Unable to execute query:"+query+"\n");
	        System.err.println(e.getMessage());
			throw(e);
		}
		
		String ret = "error man";
		if (result.next()) {
			ret = result.getString("login");
		}
		connector.closeStatement();
		connector.closeConnection();
		return ret;
	}
	
	public static List<FB> getList(int pid) throws Exception {
		Connector connector = new Connector();
		String query = "select f._text, f.fbdate, f.score, f.fid, f.login, f.pid, " +
				"(select avg(r.rating) from Rates r where r.fid = f.fid) as rating" +
				" from Feedback f" +
				" order by rating desc";
		System.err.println(query);
		ResultSet result;
		try{
			result = connector.stmt.executeQuery(query);
        } catch(Exception e) {
			System.err.println("Unable to execute query:"+query+"\n");
	        System.err.println(e.getMessage());
			throw(e);
		}
		
		List<FB> ret = new ArrayList<FB>();
		while (result.next()) {
			if (result.getInt("pid") == pid) {
				User user = new User(result.getString("login"));
				ret.add(new FB(user.getName(), result.getString("_text"), result.getString("fbdate"), 
						result.getInt("score"), result.getDouble("rating"),
						result.getInt("fid"), result.getString("login")));
			}
		}
		
		connector.closeStatement();
		connector.closeConnection();
		return ret;
	}
	
	public static boolean insertRating(String who, int fid, int rating) throws Exception {
		Connector connector = new Connector();
		
		String query = "select * from Rates "
				+ "where login = '" + who + "' and fid = " + fid;
		ResultSet result;
		try{
			result = connector.stmt.executeQuery(query);
        } catch(Exception e) {
			System.err.println("Unable to execute query:"+query+"\n");
	        System.err.println(e.getMessage());
			throw(e);
		}
		
		if (result.next()) {
			return false;
		}
		
		query = Query.insert("Rates", new Object[]{who, fid, rating});
		try{
			connector.stmt.execute(query);
        } catch(Exception e) {
			System.err.println("Unable to execute query:"+query+"\n");
	        System.err.println(e.getMessage());
			throw(e);
		}

		connector.closeStatement();
		connector.closeConnection();
		return true;
	}
}
