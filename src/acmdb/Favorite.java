package acmdb;

import java.sql.ResultSet;
import java.sql.Statement;

import utils.Checker;

public class Favorite {
	Favorite() {}
	
	static public boolean check(String login, int pid) throws Exception {
		String query;
		ResultSet results; 
		Connector connector = new Connector();
		Statement stmt = connector.stmt;
		
		query = "Select * from favorites where pid" + " = " + pid + " and login = '" + login + "'";
		try{
			results = stmt.executeQuery(query);
        } catch(Exception e) {
			System.err.println("Unable to execute query:"+query+"\n");
	        System.err.println(e.getMessage());
			throw(e);
		}
		
		boolean ret;
		if (results.next()){
			ret = true;
		} else {
			ret = false;
		}
		
		connector.closeStatement();
		connector.closeConnection();
		return ret;
	}
}
