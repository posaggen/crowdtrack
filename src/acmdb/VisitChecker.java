package acmdb;

import utils.*;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.jasper.tagplugins.jstl.core.Out;

import utils.Query;

public class VisitChecker {
	public String login, poi, date;
	public int cost;
	public int numberofheads;
	public int pid;
	
	public VisitChecker(String _login, String _poi, int _cost, int _numberofheads, String _date) {
		login = _login;
		poi = _poi;
		date = _date;
		cost = _cost;
		numberofheads = _numberofheads;
	}
	
	VisitChecker() {
		
	}
	
	public boolean checkPOI() throws Exception {

		if (Checker.isValid(poi) == false) {
			return false;
		}
		
		String query;
		ResultSet results; 
		Connector connector = new Connector();
		Statement stmt = connector.stmt;
		
		query = "Select * from POI where name" + "='" + poi + "'";
		try{
			results = stmt.executeQuery(query);
        } catch(Exception e) {
			System.err.println("Unable to execute query:"+query+"\n");
	                System.err.println(e.getMessage());
			throw(e);
		}
		
		
		boolean ret;
		if (results.next()){
			pid = Integer.parseInt(results.getString("pid"));
			System.err.println("pid = " + pid);
			ret = true;
		} else {
			ret = false;
		}
		
		connector.closeStatement();
		connector.closeConnection();
		return ret;
	}
	
	public void insert() throws Exception {
		Connector connector = new Connector();
		Statement stmt = connector.stmt;
		
		String query;
		query = "insert into VisEvent (cost, numberofheads) values(" + cost + ", " + numberofheads + ")";
		
		try{
			stmt.execute(query);
		} catch(Exception e){
			System.err.println("Unable to execute query:"+query+"\n");
            System.err.println(e.getMessage());
            throw(e);
		}
		ResultSet result = stmt.executeQuery("SELECT LAST_INSERT_ID()");
		int vid = -1;
		if (result.next()) {
			vid = Integer.parseInt(result.getString(1));
		}
		query = Query.insert("Visit", new Object[]{login, pid, vid, date});
		try{
			stmt.execute(query);
		} catch(Exception e){
			System.err.println("Unable to execute query:"+query+"\n");
            System.err.println(e.getMessage());
            throw(e);
		}
		
		connector.closeStatement();
		connector.closeConnection();
	}
};
