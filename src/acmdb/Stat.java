package acmdb;

import java.sql.ResultSet;

import utils.*;
import java.util.*;

public class Stat{
	public Stat(){
	}
	
	private HashMap<String, String> getRes(ResultSet results) throws Exception {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("pid", Integer.toString(results.getInt("pid")));
		map.put("name", results.getString("name"));
		map.put("category", results.getString("category"));
		return map;
	}
	
	public List<HashMap<String, String>> getPop() throws Exception {
		Connector connector = new Connector();
		String query = "select p.*, (select count(*) from visit v where v.pid = p.pid) as visits"
				+ " from poi p where exists (select count(*) from poi p2 where p.category = p2.category and "
				+ "((select count(*) from visit v where v.pid = p2.pid) > (select count(*) from visit v where "
				+ "v.pid = p.pid) or (select count(*) from visit v where v.pid = p.pid) is NULL or "
				+ "((select count(*) from visit v where v.pid = p2.pid) = (select count(*) from visit v where v.pid = p.pid) and "
				+ "p2.pid < p.pid)) having count(*) < 5)order by p.category, visits DESC";
		ResultSet results;
		try {
			results = connector.stmt.executeQuery(query);
		} catch (Exception e) {
			System.err.println("Unable to execute query:" + query + "\n");
			System.err.println(e.getMessage());
			throw (e);
		}
		List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		int count = 0;
		String last = "";
		while (results.next()){
			String tmp = results.getString("category");
			if (last.equals(tmp)){
				++count;
				if (count > 5){
					continue;
				}
			} else {
				count = 1;
				last = tmp;
			}
			HashMap<String, String> map = getRes(results);
			map.put("visits", Integer.toString(results.getInt("visits")));
			list.add(map);
		}
		connector.closeStatement();
		connector.closeConnection();
		return list;
	}
	
	public List<HashMap<String, String>> getCost() throws Exception {
		Connector connector = new Connector();
		String query = "select p.*, (select (sum(ve.cost) / sum(ve.numberofheads)) from visit v, visevent ve where "
				+ "ve.vid = v.vid and v.pid = p.pid) as cost from poi p where exists "
				+ "(select count(*) from poi p2 where p.category = p2.category and "
				+ "((select (sum(ve.cost) / sum(ve.numberofheads)) from visit v, visevent ve where "
				+ "ve.vid = v.vid and v.pid = p2.pid)) > ((select (sum(ve.cost) / sum(ve.numberofheads)) from visit v, visevent ve where "
				+ "ve.vid = v.vid and v.pid = p.pid)) or (((select (sum(ve.cost) / sum(ve.numberofheads)) from visit v, visevent ve where "
				+ "ve.vid = v.vid and v.pid = p2.pid)) = ((select (sum(ve.cost) / sum(ve.numberofheads)) from visit v, visevent ve where "
				+ "ve.vid = v.vid and v.pid = p.pid)) and p2.pid < p.pid) having count(*) < 5) order by p.category, cost DESC";
		ResultSet results;
		try {
			results = connector.stmt.executeQuery(query);
		} catch (Exception e) {
			System.err.println("Unable to execute query:" + query + "\n");
			System.err.println(e.getMessage());
			throw (e);
		}
		List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		int count = 0;
		String last = "";
		while (results.next()){
			String tmp = results.getString("category");
			if (last.equals(tmp)){
				++count;
				if (count > 5){
					continue;
				}
			} else {
				count = 1;
				last = tmp;
			}
			HashMap<String, String> map = getRes(results);
			map.put("cost", Double.toString(results.getDouble("cost")));
			list.add(map);
		}
		connector.closeStatement();
		connector.closeConnection();
		return list;
	}

	
	public List<HashMap<String, String>> getScore() throws Exception {
		Connector connector = new Connector();
		String query = "select p.*, (select avg(f.score) from feedback f where f.pid = p.pid) as score from poi p where "
				+ "exists (select count(*) from poi p2 where p.category = p2.category and ((select avg(f.score) from feedback f where "
				+ "f.pid = p2.pid) > (select avg(f.score) from feedback f where f.pid = p.pid) or ((select avg(f.score) from feedback f where "
				+ "f.pid = p2.pid) = (select avg(f.score) from feedback f where f.pid = p.pid) and p2.pid < p.pid)) having count(*) < 5)"
				+ "order by p.category, score DESC";
		ResultSet results;
		try {
			results = connector.stmt.executeQuery(query);
		} catch (Exception e) {
			System.err.println("Unable to execute query:" + query + "\n");
			System.err.println(e.getMessage());
			throw (e);
		}
		List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		int count = 0;
		String last = "";
		while (results.next()){
			String tmp = results.getString("category");
			if (last.equals(tmp)){
				++count;
				if (count > 5){
					continue;
				}
			} else {
				count = 1;
				last = tmp;
			}
			HashMap<String, String> map = getRes(results);
			System.out.println(results.getString("name"));
			System.out.println(results.getDouble("score"));
			map.put("score", Double.toString(results.getDouble("score")));
			list.add(map);
		}
		connector.closeStatement();
		connector.closeConnection();
		return list;
	}
	
	public static void main(String args[]) throws Exception{
		(new Stat()).getScore();
	}
	
}