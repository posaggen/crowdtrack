package acmdb;

import utils.*;
import java.util.*;

import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;

import java.sql.*;
//import javax.servlet.http.*;

public class Ranker {

	public Ranker() {
	}

	String trustRank(int m) throws Exception {
		Connector connector = new Connector();
		String query = "select u.login, u.name, ((select count(*) from trust t where t.isTrusted = 1 and t.login2 = u.login) - "
				+ "(select count(*) from trust t where t.isTrusted = 0 and t.login2 = u.login)) as score from users u order "
				+ "by score DESC limit " + Integer.toString(m);
		ResultSet results;
		try {
			results = connector.stmt.executeQuery(query);
		} catch (Exception e) {
			System.err.println("Unable to execute query:" + query + "\n");
			System.err.println(e.getMessage());
			throw (e);
		}
		String ans = "[";
		boolean isFirst = true;
		while (results.next()){
			if (!isFirst){
				ans += ",";
			}
			isFirst = false;
			ans += "{";
			ans += Json.get("login", results.getString("login"));
			ans += ",";
			ans += Json.get("name", results.getString("name"));
			ans += ",";
			ans += Json.get("score", results.getInt("score"));
			ans += "}";
		}
		ans += "]";
		connector.closeStatement();
		connector.closeConnection();
		return ans;
	}
	
	String usefulRank(int m) throws Exception {
		Connector connector = new Connector();
		String query = "select u.login, u.name, (select avg(r.rating) from feedback f, rates r where"
				+ " u.login = f.login and f.fid = r.fid) as score from users u order by score DESC limit " + Integer.toString(m);
		ResultSet results;
		try {
			results = connector.stmt.executeQuery(query);
		} catch (Exception e) {
			System.err.println("Unable to execute query:" + query + "\n");
			System.err.println(e.getMessage());
			throw (e);
		}
		String ans = "[";
		boolean isFirst = true;
		while (results.next()){
			if (!isFirst){
				ans += ",";
			}
			isFirst = false;
			ans += "{";
			ans += Json.get("login", results.getString("login"));
			ans += ",";
			ans += Json.get("name", results.getString("name"));
			ans += ",";
			ans += Json.get("score", results.getDouble("score"));
			ans += "}";
		}
		ans += "]";
		connector.closeStatement();
		connector.closeConnection();
		return ans;
	}

	public String rank(int m, int type) throws Exception {
		if (type == 0) {
			return trustRank(m);
		}
		return usefulRank(m);
	}
	
	public static void main(String args[]) throws Exception {
		Ranker ranker = new Ranker();
		System.out.println(ranker.rank(3, 1));
	}

}
