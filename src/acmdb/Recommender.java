package acmdb;

import java.sql.ResultSet;
import java.util.*;

public class Recommender{
	public Recommender(){
	}
	
	public List<HashMap<String, String>> recommend(int pid) throws Exception {
		Connector connector = new Connector();
		String query = "select p.*, (select count(*) from visit v where v.pid = p.pid and exists ("
				+ "select * from visit v1 where v1.login = v.login and v1.pid = " + Integer.toString(pid) + ")) as visits from poi p where exists ("
				+ "select * from visit v1, visit v2 where v1.login = v2.login and v1.pid = "+ Integer.toString(pid) + " and v2.pid = p.pid"
				+ ") and p.pid != " + Integer.toString(pid) + " order by visits DESC";
		ResultSet results;
		try {
			results = connector.stmt.executeQuery(query);
		} catch (Exception e) {
			System.err.println("Unable to execute query:" + query + "\n");
			System.err.println(e.getMessage());
			throw (e);
		}
		List<HashMap<String, String>> ans = new ArrayList<HashMap<String, String>>();
		while (results.next()){
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("name", results.getString("name"));
			map.put("pid", Integer.toString(results.getInt("pid")));
			map.put("visits", Integer.toString(results.getInt("visits")));
			ans.add(map);
		}
		connector.closeStatement();
		connector.closeConnection();
		return ans;
	}
	
	public static void main(String args[]) throws Exception{
		(new Recommender()).recommend(1);
	}
}