package acmdb;

import java.sql.*;
import utils.*;
import java.util.*;

public class User {

	private String login, nickname;
	private boolean isAdmin, exists;

	public User(String login) throws Exception {
		Connector connector = new Connector();
		String query = "select * from users where login = '" + login + "'";
		ResultSet results;
		try {
			results = connector.stmt.executeQuery(query);
		} catch (Exception e) {
			System.err.println("Unable to execute query:" + query + "\n");
			System.err.println(e.getMessage());
			throw (e);
		}
		if (results.next()) {
			this.login = results.getString("login");
			this.nickname = results.getString("name");
			this.isAdmin = results.getBoolean("userType");
			this.exists = true;
		} else {
			this.login = login;
			this.nickname = "fuck u no such user!!!!";
			this.exists = false;
			this.isAdmin = false;
		}
		connector.closeStatement();
		connector.closeConnection();
	}
	
	public boolean exists(){
		return this.exists;
	}

	public void trustBy(String login0) throws Exception {
		int tmp = isTrustedBy(login0);
		Connector connector = new Connector();
		String query;
		if (tmp == -1) {
			query = "insert into trust values (" + Fix.getString(login0) + "," + Fix.getString(this.login) + ",1)";
		} else {
			query = "update trust set isTrusted = 1 where login1 = " + Fix.getString(login0) + " and login2 = "
					+ Fix.getString(this.login);
		}
		try {
			connector.stmt.execute(query);
		} catch (Exception e) {
			System.err.println("Unable to execute query:" + query + "\n");
			System.err.println(e.getMessage());
			throw (e);
		}
		connector.closeStatement();
		connector.closeConnection();
	}
	
	private List<Pair<String, String>> getOneDegree(String login0) throws Exception {
		Connector connector = new Connector();
		String query = "select distinct p.pid, p.name from POI p, favorites f1, favorites f2 where "
				+ "p.pid = f1.pid and p.pid = f2.pid and f1.login = " + Fix.getString(this.login) + 
				" and f2.login = " + Fix.getString(login0);
		//System.out.println(query);
		ResultSet results;
		try {
			results = connector.stmt.executeQuery(query);
		} catch (Exception e) {
			System.err.println("Unable to execute query:" + query + "\n");
			System.err.println(e.getMessage());
			throw (e);
		}
		int total = 0;
		ArrayList<Pair<String, String>> ans = new ArrayList<Pair<String, String>>();
		while (results.next()){
			total += 1;
			ans.add(new Pair(Integer.toString(results.getInt("pid")), results.getString("name")));
		}
		if (total == 0) ans = null;
		connector.closeStatement();
		connector.closeConnection();
		return ans;
	}
	
	private List<Pair<String, String>> getTwoDegree(String login0) throws Exception {
		Connector connector = new Connector();
		String query = "select * from users u where "
				+ "u.login != " + Fix.getString(login0) + " and u.login != " + Fix.getString(this.login)
				+ " and exists (select * from POI p, favorites f1, favorites f2 where "
				+ "p.pid = f1.pid and p.pid = f2.pid and f1.login = u.login and f2.login = " + Fix.getString(login0)
				+ ") and exists (select * from POI p, favorites f1, favorites f2 where "
				+ "p.pid = f1.pid and p.pid = f2.pid and f1.login = u.login and f2.login = " + Fix.getString(this.login)
				+ ")";
		//System.out.println(query);
		ResultSet results;
		try {
			results = connector.stmt.executeQuery(query);
		} catch (Exception e) {
			System.err.println("Unable to execute query:" + query + "\n");
			System.err.println(e.getMessage());
			throw (e);
		}
		int total = 0;
		ArrayList<Pair<String, String>> ans = new ArrayList<Pair<String, String>>();
		while (results.next()){
			total += 1;
			ans.add(new Pair(results.getString("login"), results.getString("name")));
		}
		if (total == 0) ans = null;
		connector.closeStatement();
		connector.closeConnection();
		return ans;
	}
	
	
	public Pair<Integer, List<Pair<String, String>>> getDegree(String login0) throws Exception {
		List<Pair<String, String>> ans;
		ans = getOneDegree(login0);
		if (ans == null){
			ans = getTwoDegree(login0);
		} else {
			return new Pair(1, ans);
		}
		if (ans == null){
			return new Pair(0, null);
		} 
		return new Pair(2, ans);
	}

	public void distrustBy(String login0) throws Exception {
		int tmp = isTrustedBy(login0);
		Connector connector = new Connector();
		String query;
		if (tmp == -1) {
			query = "insert into trust values (" + Fix.getString(login0) + "," + Fix.getString(this.login) + ",0)";
		} else {
			query = "update trust set isTrusted = 0 where login1 = " + Fix.getString(login0) + " and login2 = "
					+ Fix.getString(this.login);
		}
		try {
			connector.stmt.execute(query);
		} catch (Exception e) {
			System.err.println("Unable to execute query:" + query + "\n");
			System.err.println(e.getMessage());
			throw (e);
		}
		connector.closeStatement();
		connector.closeConnection();
	}

	public int isTrustedBy(String login0) throws Exception {
		Connector connector = new Connector();
		String query = "select * from trust where login1 = '" + login0 + "' and login2 = '" + this.login + "'";
		ResultSet results;
		try {
			results = connector.stmt.executeQuery(query);
		} catch (Exception e) {
			System.err.println("Unable to execute query:" + query + "\n");
			System.err.println(e.getMessage());
			throw (e);
		}
		if (results.next()) {
			if (results.getBoolean("isTrusted")) {
				return 1;
			}
		} else {
			return -1;
		}
		connector.closeStatement();
		connector.closeConnection();
		return 0;
	}
	
	public String reset(String login0) throws Exception {
		Connector connector = new Connector();
		String query = "delete from trust where login1 = '" + login0 + "' and login2 = '" + this.login + "'";
		System.out.println(query);
		ResultSet results;
		try {
			connector.stmt.execute(query);
		} catch (Exception e) {
			System.err.println("Unable to execute query:" + query + "\n");
			System.err.println(e.getMessage());
			throw (e);
		}
		connector.closeStatement();
		connector.closeConnection();
		return query;
	}

	public String getLogin() {
		return this.login;
	}

	public String getName() {
		return this.nickname;
	}

	public boolean isAdmin() {
		return this.isAdmin;
	}
	
	public static void main(String args[])throws Exception{
		User u = new User("admin");
		u.reset("wengjian");
	}

};
